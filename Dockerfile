from ubuntu:14.04
RUN apt-get update && apt-get install -y dnsmasq wget
RUN wget -qO- https://get.docker.com/ | sh
ADD files/docker-dns /etc/dnsmasq.d/docker-dns
ADD files/docker /etc/default/docker
ADD files/start.sh /start.sh
ADD files/update-docker-dns.sh /bin/update-docker-dns.sh
ADD files/monitor_docker_addresses.sh	/bin/monitor_docker_addresses.sh
ENTRYPOINT [ "/start.sh" ]

